package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.dao.TelefoneRepository;
import br.com.ciandt.dojoTurma2.entity.Telefone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TelefoneService implements ITelefoneService {

    @Autowired
    TelefoneRepository telefoneRepository;

    @Override
    public List<Telefone> findAll() {
        return telefoneRepository.findAll();
    }

    @Override
    public Optional<Telefone> findById(Long id) {
        return telefoneRepository.findById(id);
    }

    @Override
    public List<Telefone> findByContatoId(Long contatoID) {
        return telefoneRepository.findByContatoId(contatoID);
    }

    public Telefone saveOrUpdate(Telefone addTelefone) {
        return telefoneRepository.save(addTelefone);
    }

    public boolean deleteById(Long id){
        telefoneRepository.deleteById(id);
        return !telefoneRepository.existsById(id);
    }

}
