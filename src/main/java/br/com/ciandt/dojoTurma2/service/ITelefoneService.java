package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.entity.Telefone;

import java.util.List;
import java.util.Optional;

public interface ITelefoneService {

    List<Telefone> findAll();

    Optional<Telefone> findById(Long id) ;

    List<Telefone> findByContatoId(Long contatoId);

    Telefone saveOrUpdate(Telefone telefone);

    boolean deleteById(Long id);
}
