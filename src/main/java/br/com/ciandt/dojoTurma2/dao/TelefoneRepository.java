package br.com.ciandt.dojoTurma2.dao;

import br.com.ciandt.dojoTurma2.entity.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TelefoneRepository extends JpaRepository<Telefone, Long> {

    List<Telefone> findByContatoId(Long contatoId);
}

