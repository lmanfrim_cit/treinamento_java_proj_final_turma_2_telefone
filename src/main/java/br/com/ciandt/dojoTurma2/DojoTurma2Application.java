package br.com.ciandt.dojoTurma2;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@SpringBootApplication
@EnableEurekaClient
public class DojoTurma2Application {

	public static void main(String[] args) {
		SpringApplication.run(DojoTurma2Application.class);
	}

}
