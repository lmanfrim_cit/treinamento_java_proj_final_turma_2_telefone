package br.com.ciandt.dojoTurma2.controller;

import br.com.ciandt.dojoTurma2.entity.Telefone;
import br.com.ciandt.dojoTurma2.service.TelefoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/telefone")
public class TelefoneController {

    @Autowired
    //@Qualifier(value = "telefoneService")
            TelefoneService telefoneService;

    @GetMapping
    public ResponseEntity<List<Telefone>> telefones() {
        return new ResponseEntity<>(
                telefoneService.findAll(), HttpStatus.BAD_GATEWAY);
    }

    /*public List<Contato> contatos() {
        return telefoneService.findAll();
    }*/

    @GetMapping("/contato/{id}")
    public List<Telefone> contatoById(@PathVariable Long id) {
        return telefoneService.findByContatoId(id);
    }

    @GetMapping("/{id}")
    //@ResponseStatus()
    public Optional<Telefone> telefoneById(@PathVariable Long id) {
        return telefoneService.findById(id);
    }

    @PostMapping
    public Telefone add(@RequestBody Telefone contato) {
        return telefoneService.saveOrUpdate(contato);
    }

    @DeleteMapping("{id}")
    public boolean deleteById(@PathVariable Long id) {
        return telefoneService.deleteById(id);
    }
}
